# Material Curso Certificacao Cisco CCNA  - Senai Taguatinga
##Prof. Clesio Santos

###Conteudo:
------

| Dia         | Aula          | Conteudo                                                  |
|:-----------:|:-------------:|:----------------------------------------------------------|
| 01/02/2016  |01             | Apresentacao Turma, Apresentação do Curso e Visao         |
| 02/02/2016  |02             | Explorando a Redes                                        |
| 03/02/2016  |03             |                         **`REVISAO`**                        |
| 04/02/2016  |04             | Configuração de um sistema operacional de rede            | 
| 05/02/2016  |05             | Protocolos  e comunicações de rede                        | 
| 11/02/2016  |06             | Acesso à rede                                             |
| 12/02/2016  |07             |                         **`ATIVIDADE`**                      |
| 15/02/2016  |08             | Ethernet                                                  | 
| 16/02/2016  |09             |                         **`ATIVIDADE`**                      |
| 17/02/2016  |10             | Camada de Rede                                            | 
| 18/02/2016  |11             |                         **`ATIVIDADE`**                      |
| 19/02/2016  |12             | Camada de Transport                                       | 
| 22/02/2016  |13             | Enderećamento IP                                          |
| 23/02/2016  |00             |                         **`NAO HOUVE AULA`**                 |
| 24/02/2016  |14             |                         **`ATIVIDADE`**                      |
| 25/02/2016  |15             | IPV6                                                      | 
| 26/02/2016  |16             |                         **`ATIVIDADE`**                      |
| 29/02/2016  |17             | Divisão de redes IP em sub-redes                          | 
| 01/03/2016  |18             |                         **`ATIVIDADE`**                      |
| 02/03/2016  |19             |                         **`REVISAO`**                        |
| 03/03/2016  |20             | Camada de aplicação                                       | 
| 04/03/2016  |21             | É uma rede                                                | 
| 07/03/2016  |22             |                     **`PROVA CCNA1`**                        |
| 29/10/2015  |17             | Redes Convergentes e Redes Comutadas                      |
| 30/10/2015  |18             | Configuração Básica de Switch                             | 
| 03/11/2015  |19             | Segmentacao VLAN                                          | 
| 04/11/2015  |20             |                         **`ATIVIDADE`**                      |
| 05/11/2015  |21             | Conceitos de Roteamento                                   | 
| 06/11/2015  |22             | Roteamento Inter-VLANS                                    | 
| 09/11/2015  |23             | Roteamento Estático                                       | 
| 10/11/2015  |24             | Roteando Dinamicamente                                    | 
| 11/11/2015  |25             | OSPF de Área Única                                        |
| 12/11/2015  |26             |                         **`ATIVIDADE`**                      |
| 13/11/2015  |27             | Listas de Controle de Acesso                              |
| 14/11/2015  |28             |                     **`ATIVIDADE EXTRA`**                    |
| 16/11/2015  |29             | DHCP                                                      | 
| 17/11/2015  |30             | NAT para IPv4                                             | 
| 18/11/2015  |31             | Implementing a Network Design                             | 
| 19/11/2015  |32             | LAN Redundancy                                            | 
| 20/11/2015  |33             | Link Aggregation                                          | 
| 21/11/2015  |34             |                     **`ATIVIDADE EXTRA`**                    |
| 23/11/2015  |35             | Wireless LANs                                             |
| 24/11/2015  |36             | Adjust and Troubleshoot Single-Area OSPF                  |
| 25/11/2015  |37             | Multiarea OSPF                                            |
| 26/11/2015  |38             |                         **`ATIVIDADE`**                      |
| 27/11/2015  |39             | Enhanced Interior Gateway Protocol (EIGRP)                |
| 01/12/2015  |40             | EIGRP Advanced Configurations and Troubleshooting         |
| 02/11/2015  |41             | IOS Images and Licensing                                  |

1. Aula 01 
    * Aula Inaugural
    * Apresentacao
    * O que é Rede?
2. Aula 02
    * Definicao de Comunicacao = Entendimento + Movimentacao
    * Quais as ferramentas necessarias durante o curso?
    * Bibliografias recomendadas
3. Aula 03
    * Componentes de Rede
    * Modelos de Rede Cliente-Servidor, Peer-to-peer, ponto-a-ponto
    * LAN e WAN

    * Modelo OSI
4. Aula 04
    * Explorando Redes
5. Aula 05
6. Aula 06



