en
conf t
hostname R1
enable secret class
line console 0
password cisco
login
exit
line vty 0 15
password cisco
login
exit
service password-encryption
wr

interface serial 0/3/1
ip address 172.16.28.33 255.255.255.252
no shutdown
exit

interface serial 0/3/0
ip address 176.16.28.46 255.255.255.252
no shutdown
exit

interface fastEthernet 0/1
ip address 172.16.24.1 255.255.252.0
no shutdown
exit

router ospf 1
router-id 1.1.1.1 
network 172.16.24.0 0.0.3.255 area 0
network 172.16.28.44 0.0.0.3 area 0
network 172.16.28.32 0.0.0.3 area 0